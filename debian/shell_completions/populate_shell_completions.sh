#!/bin/bash

seqkit genautocomplete --shell fish --file debian/shell_completions/seqkit.fish
seqkit genautocomplete --shell bash --file debian/shell_completions/seqkit
seqkit genautocomplete --shell zsh --file debian/shell_completions/_seqkit

echo "DONE"
